
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 
const getDeck = () => {

  
    var deck = new Array();
    let suits=['Hearts', 'Clubs','Diamonds','Spades']
    let faces=['Jack', 'Queen','King','Ace']

  
    for(var i = 0; i < suits.length; i++)
    {
      for(var x = 2; x <= 10; x++)
      {
        var card = {
          val: x,
           displayVal:x.toString(),
           Suit: suits[i],
          };
        deck.push(card);
      }

    for(let face=0;face<faces.length;face++){
    let val=faces[face]==='Ace' ? 11 : 10
    
    var card = {
      val,
       displayVal: faces[face],
       Suit: suits[i],
      };
      deck.push(card);

    }

    }
  
    return deck;
  }





 CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
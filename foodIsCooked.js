
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
  
const foodIsCooked = function(kind, internalTemp, doneness) {

     if (kind==='chicken' && internalTemp >165){

      return true
     }
     else return false
     else if (kind=== 'beef') 
     {
        if (doneness === 'rare' && internalTemp >125){

          return true
        }
        else if (doneness === 'meduim' && internalTemp >135){

          return true
        }
        else if (doneness ==='well' && internalTemp >155){

          return true
        }
        else return false
     }
     else return false

}



 Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true


const FOOD_MATRIX = {
  "chicken": 165,
  "beef": {
      "rare": 125,
      "medium": 135,
      "well": 155
    }
};

const foodIsCooked = function(kind, internalTemp, doneness) {
  let isCooked = false;

  if(doneness) {
    isCooked = FOOD_MATRIX[kind][doneness] < internalTemp ? true : false;
    console.log(`Checking ${FOOD_MATRIX[kind][doneness]} against ${internalTemp}, is food cooked? ${isCooked}`);

  } else {
    isCooked = FOOD_MATRIX[kind] < internalTemp ? true : false;
    console.log(`Checking ${FOOD_MATRIX[kind]} against ${internalTemp}, is food cooked? ${isCooked}`);
  }

  return isCooked;
}
